﻿using System.ComponentModel.DataAnnotations;

namespace CodeFirst.Models
{
    public class Accounts
    {
        [Key]
        public int AccountId { get; set; }
        public string CustomerId { get; set; }
        public string AccountName { get; set; }
        //

        public virtual ICollection<Customer> Customers { get; set; }
        public virtual ICollection<Transactions> Transaction { get; set; }
    }
}
