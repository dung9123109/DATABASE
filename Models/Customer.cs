﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Net.NetworkInformation;


namespace CodeFirst.Models
{
    public class Customer
    {
        [Key]
        public int CustomerID { get; set; }
        [Required, StringLength(100)]
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string Address { get; set; }
        public string? Contact { get; set; }
        public string UserName {  get; set; }
        public string Password { get; set; }

        //Khoa ngoai den bang Account
        [ForeignKey("Account")]
        public int AccountId { get; set; }
        public virtual Accounts Account { get; set; }

        

    }
}
