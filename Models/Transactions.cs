﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CodeFirst.Models
{
    public class Transactions
    {
        [Key]
        public string TransactionalId { get; set; }
        public string EmployeeId { get; set; }
        public string CustomerId { get; set; }
        public string? Name {  get; set; }

        //Khoa ngoai den bang account
        [ForeignKey("Account")]
        public int AccountId {  get; set; }
        public virtual Accounts Account { get; set; } 

    }
}
