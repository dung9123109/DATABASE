﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CodeFirst.Models
{
    public class Reports
    {
        [Key]
        public int ReportId { get; set; }
        public string AccountId { get; set; }
        public string LogsId {  get; set; }
        public string ReportName { get; set; }
        public DateTime ReportDate {  get; set; }

        //Khoa ngoai den bang Employees
        [ForeignKey("Employees")]
        public int EmployeeId { get; set; }
        public virtual Employees Employees { get; set; }
    }
}
