﻿using System.ComponentModel.DataAnnotations;

namespace CodeFirst.Models
{
    public class Logs
    {
        [Key]
        public int LogsId { get; set; }
        public string TransactionalId { get; set; }
        public DateTime LoginDate {  get; set; }
        public DateTime LoginTime { get; set;}
    }
}
