﻿using System.ComponentModel.DataAnnotations;

namespace CodeFirst.Models
{
    public class Employees
    {
        [Key]
        public int EmployeesId { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string Address { get; set; }
        public string? Contact { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Reports { get; set; }
        public virtual ICollection<Reports> Report { get; set; }
    }
}
