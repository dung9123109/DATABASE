﻿using CodeFirst.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace CodeFirst.Data.Migrations
{
    public class BankingDbContext : DbContext
    {
        public BankingDbContext(DbContextOptions<BankingDbContext> options)
            : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Customer>();
            modelBuilder.Entity<Employees>();
            modelBuilder.Entity<Reports>();
            modelBuilder.Entity<Transactions>();
            modelBuilder.Entity<Logs>();
            modelBuilder.Entity<Accounts>();

            modelBuilder.Entity<Customer>().HasKey(c => c.CustomerID);
            modelBuilder.Entity<Employees>().HasKey(c => c.EmployeesId);
            modelBuilder.Entity<Reports>().HasKey(c => c.ReportId);
            modelBuilder.Entity<Transactions>().HasKey(c => c.TransactionalId);
            modelBuilder.Entity<Logs>().HasKey(c=> c.LogsId);
            modelBuilder.Entity<Accounts>().HasKey(c => c.AccountId);

            //Cac quan he 1-n
            modelBuilder.Entity<Customer>()
                .HasOne(c => c.Account)
                .WithMany(a => a.Customers)
                .HasForeignKey( c => c.AccountId );

           modelBuilder.Entity<Reports>()
               .HasOne(r => r.Employees)
               .WithMany(b => b.Report)
               .HasForeignKey(r => r.EmployeeId);

            modelBuilder.Entity<Transactions>()
                .HasOne(t => t.Account)
                .WithMany(a => a.Transaction)
                .HasForeignKey(t => t.AccountId);

        }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Employees> Employees { get; set; }
        public DbSet<Reports> Report { get; set; }
        public DbSet<Transactions> Transactions { get; set; }
        public DbSet<Logs> Logs { get; set; }
        public DbSet<Accounts> Accounts { get; set; }   
    }
}
